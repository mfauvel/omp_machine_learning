\subsection{Motivations}
\begin{frame}[label={sec:org2ec5396}]{Illustration}
  \begin{itemize}
  \item <1-> \alert{Curse of dimensionality}: it is not possible to get enough data to cover all the observation space.
    \begin{center}
      \emph{High dimensional spaces are mostly empty !}
    \end{center}
  \item <2> Multivariate data live in a lower dimensional space, but which one ?
    \begin{center}
      \begin{tabular}{cc}
        \begin{tikzpicture}
          \begin{axis}[grid=major,small]
            \addplot3 [mesh, samples=15, domain=-5:5] {x+y+1};
          \end{axis}
        \end{tikzpicture}&\begin{tikzpicture}
          \begin{axis}[grid=major,small]
            \addplot3 [mesh, samples=15, domain=-5:5] {x*x-2*y+1};
                     \end{axis}
                   \end{tikzpicture}     
      \end{tabular}
    \end{center}
  \end{itemize}
\end{frame}

\begin{frame}[label={sec:org60ebfcb}]{Application}
  \begin{itemize}
  \item Feature extraction is important in machine learning because:
    \begin{itemize}
    \item It reduces the size of the data,
    \item It limits the redundancy,
    \item It permits visualization of the data,
    \item It mitigates the \emph{curse of dimensionality}.
    \end{itemize}
  \item Extraction techniques:
    \begin{itemize}
    \item Physically based method,
    \item Statistical methods,
    \item Linear and non linear filters.
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Unsupervised feature extraction}
\label{sec:org4cef40b}
\begin{frame}[label={sec:org3859b61}]{Principal Components Analysis}
\begin{itemize}
\item Linear transformation used to reduce the dimensionality of the data.
$$ z_i = \langle\mathbf{v}_i,\mathbf{x}\rangle$$
\item Find features \(\mathbf{z}\) that  account for most of the variability of the data:
\begin{itemize}
\item \(z_1,~z_2,~z_3,\ldots\) are mutually uncorrelated,
\item \(\text{var}(z_i)\) is as large as possible,
\item \(\text{var}(z_1)>\text{var}(z_2)>\text{var}(z_3)>\ldots\)
\end{itemize}
\end{itemize}

\begin{center}
  \begin{tikzpicture}
    \begin{axis}[grid,small,width=0.4\linewidth,height=0.32\linewidth,xmin=0,xmax=2.5,ymin=0,ymax=2]
      \addplot[only marks,blue] table[x index=0,y index = 1,col sep =comma] {./Figs/FE/pca_data.csv};
      \begin{scope}
        \only<2>{\addplot[very thick,red] coordinates { (0.080264,0.83834891)  (2.06023219,1.12070676)}};
      \end{scope}
    \end{axis}                                  
  \end{tikzpicture}
\end{center}
\end{frame}
\begin{frame}[label={sec:org107c944}]{Maximization of the variance 1/2}
\begin{itemize}
\item <1-> Search \(\mathbf{v}_1\) such as \(\max\text{var}(z_1)\):
\begin{eqnarray*}
  \text{var}(z_1) & = & \text{var}(\langle\mathbf{v}_1,\mathbf{x}\rangle)\\
  &=& \mathbf{v}_1^\top\boldsymbol{\Sigma}\mathbf{v}_1
\end{eqnarray*}
\item <2-> Indetermined: if \(\hat{\mathbf{v}}_1\) maximizes the variance, \(\alpha\hat{\mathbf{v}}_1\) too!  Add a constraint: \(\langle\mathbf{v}_1,\mathbf{v}_1\rangle=1\)
\item <3-> Lagrangian:
\begin{eqnarray*}
  \mathcal{L}(\mathbf{v}_1,\lambda_1) = \mathbf{v}_1^\top\boldsymbol{\Sigma}\mathbf{v}_1 + \lambda_1(1- \mathbf{v}_1^\top\mathbf{v}_1)  
\end{eqnarray*}
\item <4-> Compute the derivative w.r.t \(\mathbf{v}_1\):
\begin{eqnarray*}
\frac{\partial\mathcal{L}}{\partial\mathbf{v}_1} = 2\boldsymbol{\Sigma}\mathbf{v}_1-2\lambda_1\mathbf{v}_1
\end{eqnarray*}
\item <5-> \(\mathbf{v}_1\) is an eigenvector of the covariance matrix of \(\mathbf{x}\):
\begin{eqnarray*}
  \boldsymbol{\Sigma}\mathbf{v}_1 =\lambda_1\mathbf{v}_1
\end{eqnarray*}
\item <6->  \(\mathbf{v}_1\) is the eigenvector corresponding to the largest eigenvalues !
\begin{eqnarray*}
  \text{var}(z_1)  =  \mathbf{v}_1^\top\boldsymbol{\Sigma}\mathbf{v}_1 = \lambda_1 \mathbf{v}_1^\top\mathbf{v}_1 = \lambda_1
\end{eqnarray*}
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org77f1e4c}]{Maximization of the variance 2/2}
\begin{itemize}
\item <1-> Search \(\mathbf{v}_2\) such as \(\max\text{var}(z_2)\) and \(\langle\mathbf{v}_2,\mathbf{v}_2\rangle=1\) and \(\langle\mathbf{v}_1,\mathbf{v}_2\rangle=0\)
\item <2-> Lagrangian:
\begin{eqnarray*}
  \mathcal{L}(\mathbf{v}_2,\lambda_2,\beta_1) = \mathbf{v}_2^\top\boldsymbol{\Sigma}\mathbf{v}_2 + \lambda_2(1- \mathbf{v}_2^\top\mathbf{v}_2) + \beta_1(0 - \mathbf{v}_2^\top\mathbf{v}_1)
\end{eqnarray*}
\item <3-> Compute the derivative w.r.t \(\mathbf{v}_2\):
\begin{eqnarray*}
\frac{\partial\mathcal{L}}{\partial\mathbf{v}_2} &=& 2\boldsymbol{\Sigma}\mathbf{v}_2-2\lambda_2\mathbf{v}_2-\beta_1\mathbf{v}_1\\
\boldsymbol{\Sigma}\mathbf{v}_2 &=& \lambda_2\mathbf{v}_2+2\beta_1\mathbf{v}_1
\end{eqnarray*}
\item <4-> At optimality, \(\langle\mathbf{v}_1,\mathbf{v}_2\rangle=0\). Left-multiplying by \(\mathbf{v}_1^\top\) the above equation:
\begin{eqnarray*}
  \mathbf{v}_1^\top\boldsymbol{\Sigma}\mathbf{v}_2 &=& 2\beta_1 \\
  \lambda_1\mathbf{v}_1^\top\mathbf{v}_2 &=& 2\beta_1 \\
  0 &=& 2\beta_1 \\
\end{eqnarray*}
\item <5-> Hence, we have 
\begin{eqnarray*}
  \boldsymbol{\Sigma}\mathbf{v}_2 =\lambda_2\mathbf{v}_2
\end{eqnarray*}
\item <6-> \(\mathbf{v}_2\) is the eigenvector corresponding the \emph{second largest} eigenvalues
\item <7-> \(\mathbf{v}_k\) is the eigenvector corresponding the \emph{\(k^{\text{th}}\) largest} eigenvalues
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org9f31f75}]{PCA in practice}
\begin{enumerate}
\item Empirical estimation the mean value:
\begin{eqnarray*}
  \boldsymbol{\mu} = \frac{1}{n}\sum_{i=1}^n\mathbf{x}_i
\end{eqnarray*}
\item Empirical estimation the covariance matrix:
\begin{eqnarray*}
  \boldsymbol{\Sigma} = \frac{1}{n-1}\sum_{i=1}^n(\mathbf{x}_i-\boldsymbol{\mu})(\mathbf{x}_i-\boldsymbol{\mu})^\top
\end{eqnarray*}
\item Compute \(p\) first eigenvalues/eigenvectors\ldots{} How to choose \(p\) ? Explained variance: 
$$\frac{\sum_{i=1}^d\lambda_i}{\sum_{i=1}^p\lambda_i}$$
\end{enumerate}

\emph{Note}: Standardization/scaling matters! 
\end{frame}

\subsection{Supervised feature extraction}
\label{sec:orge0a66f6}
\begin{frame}[label={sec:org8dec193}]{Fisher's Discriminant Analysis}
\begin{itemize}
\item We observe some \(\{\mathbf{x}_i,y_i\}_{i=1}^n\)
\item Use the label information to find the linear features that highlight differences among classes

\begin{center}
  \begin{tikzpicture}
    \begin{axis}[width=0.6\textwidth,grid,small,xmin=-5,xmax=5,ymin=-5,ymax=5]
      \addplot[scatter, only marks,scatter src=explicit]table[col sep=comma,meta index=2,x index =0,y index=1] {./Figs/FE/lda_data.csv};      
      \only<2>{\addplot[domain=-5:5,very thick] {-x/0.52306077251960925*0.85229538790913895 - 3.65687201/3.04406312};}
    \end{axis}
  \end{tikzpicture}
\end{center}
\item FDA: find \(\mathbf{a}\) such as the ratio between the \emph{between projected variance} and the \emph{sample projected variance} is maximal
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org8209be7}]{FDA Algorithm}
\begin{itemize}
\item Between-class covariance matrix:
\begin{eqnarray*}
  \mathbf{B} = \frac{1}{n}\sum_{k=1}^Kn_k(\boldsymbol{\mu}_k-\boldsymbol{\mu})(\boldsymbol{\mu}_k-\boldsymbol{\mu})^\top
\end{eqnarray*}
\item Class covariance matrix
\begin{eqnarray*}
  \boldsymbol{\Sigma}_k = \frac{1}{n_k-1}\sum_{i=1,i \in k}^{n_k}(\mathbf{x}_i-\boldsymbol{\mu}_k)(\mathbf{x}_i-\boldsymbol{\mu}_k)^\top
\end{eqnarray*}
\item Within-class covariance matrix
\begin{eqnarray*}
  \mathbf{W} = \sum_{k=1}^K\boldsymbol{\Sigma}_k
\end{eqnarray*}
\item The Fisher discriminant subspace is given by the eigenvectors of \(\mathbf{W}^{(-1)}\mathbf{B}\)
\item Remark: there are at most \(K-1\) eigenvectors because \(\text{Rank}(\mathbf{B})\leq K-1\). They should be selected similarly to PCA.
\item There is an equivalence between FDA and LDA
\item \textcolor{red}{Notebook}
\end{itemize}
\end{frame}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
