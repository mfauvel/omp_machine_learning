# Introductory course on machine learning for classification

The document /main.pdf/ contains an introductory course on machine learning for classification given by Florent Chatelain and Mathieu Fauvel. Several ipython notebooks are also provided below and the repository /Notebooks/

# Running ML example notebooks inside of Jupyter on mybinder.org

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fmfauvel%2Fomp_machine_learning/77d246991186d758147fc280eeef0eed563d525f?filepath=Notebooks)

# Citation
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1920227.svg)](https://doi.org/10.5281/zenodo.1920227)

